import bodyParser from 'body-parser';
import config from 'config';
const mongoose = require('./src/mongoose')
const express = require('express');
const app = express()
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use('/',require('./src/router/index'));

const server = app.listen(config.get('port'),()=>{
    console.log(`[API] Server ready at localhost:${config.get('port')}`)
})