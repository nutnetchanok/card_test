import _ from 'lodash';

const cardDB = require('../model/cardDB');

export async function addCard (req){
    const {
        name,
        status,
        content,
        category,
        author
    } = _.get(req,'body')
    try {
        const result = {
            name,
            status,
            content,
            category,
            author
        }
        console.log(result);
        
        await cardDB.create(result);
        return {
            status:'200',
            message:'create sucess',
            result
        }
    } catch (error) {
        return {
        status:'400',
        message:error.message
    }
    }
    
}
export async function editCard (req){
    try {
        const {name,author}= _.get(req,'body')
        const check = await cardDB.findOne({name});
        if(check.author === author){
        
        const result = {
            $set:{
                ...req.body
            }
        }
        console.log(result);
        
        await cardDB.findOneAndUpdate({name},result)
        return {
            status:'200',
            message:'edit sucess',
            result
        }
    }
    else{
        return {
            status:'400',
            message:'edit fail author is not correct',
        }
    }
        
    } catch (error) {
        return {
            status:'400',
            message:error.message
        
    }
}
}
export async function deleteCard (req){
    const {
        name,
        author
    } = _.get(req,'body')
    
try {
    const name = await cardDB.findOne({name});
    if(check.author === author){
        await cardDB.findOneAndDelete({name})
        return {
            status:'200',
            message:'create sucess',
            result
        }
    }
    else{
        return {
            status:'400',
            message:'delete fail author is not correct',
        }
        
    }
} catch (error) {
    return {
        status:'400',
        message:error.message
    }
                 }
}

export default {
    addCard,
    editCard,
    deleteCard
};