import _ from 'lodash';
 
import {addCard,editCard,deleteCard} from '../service/card'

const express = require('express');
const app = express();


app.post('/addCard',async(req,res)=>{
    try {
        const result = await addCard(req)
        res.status(200).send(result);
        
    } catch (error) {
        res.status(401).send('error')
    }
})
app.patch('/editCard',async(req,res)=>{
    try {
        const result = await editCard(req)
        res.status(200).send(result);
        
    } catch (error) {
        res.status(401).send('error')
    }
})
app.delete('/deleteCard',async(req,res)=>{
    try {
        const result = await deleteCard(req)
        res.status(200).send(result);
        
    } catch (error) {
        res.status(401).send('error')
    }
})

module.exports = app;