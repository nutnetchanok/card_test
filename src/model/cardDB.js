import mongoose from 'mongoose';

const {Schema} = mongoose;
const cardDB = new Schema({
    name:{
        type: String,
        required:true,
        unique:true
    },
    status:{
        type: String,
        required:true
    },
    content:{
        type: String,
        required:true
    },
    category:{
        type: String,
        required:true
    },
    author:{
        type:String,
        required:true
    }
},{timestamps:true});

 module.exports = mongoose.model('cardDB',cardDB)